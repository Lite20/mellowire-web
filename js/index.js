var questions = [
    "the washroom",
    "room #42",
    "the gym",
    "the fire exit",
    "room #259",
    "the meeting room",
    "the office",
    "the library",
    "the elevator",
    "room #310",
    "the art room",
    "the kitchen"
];

var state = 0;
var pos = 0;

function update() {
    if (state === 0) {
        // display question
        typeout();
    } else if (state === 1) {
        curr = document.getElementById('question-line').innerHTML;
        curr = curr.slice(0, -1) + '&nbsp;&nbsp;';
        document.getElementById('question-line').innerHTML = curr;
        state++;
        setTimeout(update, 500);
    } else if (state === 2) {
        curr = document.getElementById('question-line').innerHTML;
        curr = curr.slice(0, -12) + '_';
        document.getElementById('question-line').innerHTML = curr;
        state++;
        setTimeout(update, 500);
    } else if (state === 3) {
        curr = document.getElementById('question-line').innerHTML;
        curr = curr.slice(0, -1) + '&nbsp;&nbsp;';
        document.getElementById('question-line').innerHTML = curr;
        state++;
        setTimeout(update, 500);
    } else if (state === 4) {
        curr = document.getElementById('question-line').innerHTML;
        curr = curr.slice(0, -12) + '_';
        document.getElementById('question-line').innerHTML = curr;
        state++;
        setTimeout(update, 500);
    } else if (state === 5) {
        curr = document.getElementById('question-line').innerHTML;
        curr = curr.slice(0, -1) + '&nbsp;&nbsp;';
        document.getElementById('question-line').innerHTML = curr;
        state++;
        setTimeout(update, 500);
    } else if (state === 6) {
        curr = document.getElementById('question-line').innerHTML;
        curr = curr.slice(0, -12) + '_';
        document.getElementById('question-line').innerHTML = curr;
        state++;
        setTimeout(update, 500);
    } else if (state === 7) {
        // clear question
        typeoff();
    } else {
        pos++;
        if (pos == questions.length) {
            pos = 0;
        }

        state = 0;
        setTimeout(update, 500);
    }
}

var place = 0;
var curr = "_";

function typeout() {
    if (place < questions[pos].length) {
        curr = document.getElementById('question-line').innerHTML;
        curr = curr.slice(0, -1);
        curr += questions[pos][place] + '_';
        document.getElementById('question-line').innerHTML = curr;
        place++;
        setTimeout(typeout, randNum(50, 150));
    } else {
        state++;
        setTimeout(update, 250);
    }
}

function typeoff() {
    if (place > 0) {
        curr = document.getElementById('question-line').innerHTML;
        curr = curr.slice(0, -2) + '_';
        document.getElementById('question-line').innerHTML = curr;
        place--;
        setTimeout(typeoff, 50);
    } else {
        curr = document.getElementById('question-line').innerHTML;
        curr = curr.slice(0, -1) + '_';
        document.getElementById('question-line').innerHTML = curr;
        state++;
        setTimeout(update, 250);
    }
}

function randNum(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

document.onreadystatechange = function () {
    if (document.readyState === 'complete') {
        update();
    }
};
