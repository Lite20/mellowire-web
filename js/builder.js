var scroller = document.getElementById('zoom');
var canvas = document.getElementById('map');
var ctx, metadata, scale;
var floors = [];
var floor = 0;
var slideX = 0;
var slideY = 0;
var touch1Pos, touch2Pos;
var roomX, roomY, hallX, hallY, title, last;

// create the editor
var container = document.getElementById("jsoneditor");
var options = {};
var editor = new JSONEditor(container, options);

canvas.addEventListener("mousedown", function (e) {
    var ratioX = canvas.width / canvas.offsetWidth;
    var ratioY = canvas.height / canvas.offsetHeight;
    if (e.shiftKey) {
        roomX = (e.clientX * ratioX) / floors[floor].width;
        roomY = (e.clientY * ratioY) / floors[floor].height;
    } else {
        hallX = (e.clientX * ratioX) / floors[floor].width;
        hallY = (e.clientY * ratioY) / floors[floor].height;
        title = prompt("Room ID / Number");
        metadata.floor[1][title] = {
            x: hallX,
            y: hallY,
            rmX: roomX,
            rmY: roomY,
            connected: []
        };

        if (typeof last !== 'undefined') {
            metadata.floor[1][title].connected.push(last);
            metadata.floor[1][last].connected.push(title);
        }

        last = title;
        editor.set(metadata);
    }
}, false);

function init() {
    ctx = canvas.getContext('2d');
    var building_code = getQueryVariable('bc');
    if (building_code) {
        loadFloors(building_code);
    } else {
        findLocation();
    }
}

function setCanvas() {
    ctx.canvas.width = floors[floor].width;
    ctx.canvas.height = floors[floor].height;
}

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }

    return false;
}

function httpGetAsync(theUrl, callback) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            callback(JSON.parse(xmlHttp.responseText));
        }
    };

    xmlHttp.open("GET", theUrl, true); // true for asynchronous
    xmlHttp.send(null);
}

function loadFloors(id) {
    metadata = {
        floor: {
            1: {}
        }
    };

    for (var i = 0; i < Object.keys(metadata.floor).length; i++) {
        var img = new Image();
        img.onload = function () {
            floors.push(img);
            setCanvas();
        };

        img.src = 'http://mellowire.com:8080/' + id + '' + (i + 1) + '.png';
    }

    editor.set(metadata);
    launch();
}

function launch(timestamp) {
    window.requestAnimationFrame(launch);
    if (typeof floors[floor] == 'undefined') {
        return;
    }

    metadata = editor.get();

    // clear the view
    ctx.fillStyle = '#ffffff';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    // draw the map
    ctx.drawImage(
        floors[floor],
        0,
        0,
        floors[floor].width,
        floors[floor].height
    );

    for (var name in metadata.floor[floor + 1]) {
        var dot = metadata.floor[floor + 1][name];
        ctx.fillStyle = "#00FF00";
        ctx.beginPath();
        ctx.arc(floors[floor].width * dot.x,
            floors[floor].height * dot.y,
            32,
            0,
            2 * Math.PI,
            false
        );

        ctx.fill();
        ctx.fillStyle = "#0000FF";
        ctx.beginPath();
        ctx.arc(floors[floor].width * dot.rmX,
            floors[floor].height * dot.rmY,
            32,
            0,
            2 * Math.PI,
            false
        );

        ctx.fill();

    }

    ctx.beginPath();
    ctx.lineWidth = 5;
    ctx.strokeStyle = 'blue';
    for (var name in metadata.floor[floor + 1]) {
        var dot = metadata.floor[floor + 1][name];
        for (var i = 0; i < dot.connected.length; i++) {
            var pdot = metadata.floor[floor + 1][dot.connected[i]];
            ctx.beginPath();
            ctx.moveTo(
                dot.x * floors[floor].width,
                dot.y * floors[floor].height
            );
            ctx.lineTo(
                pdot.x * floors[floor].width,
                pdot.y * floors[floor].height
            );

            ctx.stroke();
        }
    }

    // TODO draw the path to destination
    // request next frame
}

function findLocation() {
    httpGetAsync('http://mellowire.com:8080/api/servicelist/', function (resp) {
        if (resp.s !== 'suc') {
            alert("Oh no! That's an invalid link... Error: " + resp.c);
            return;
        } else {
            var data_list = '<input id="place_input" list="locations" name="locations"><datalist id="locations">';
            for (var i = 0; i < resp.list.rows.length; i++) {

                console.log(resp.list.rows);
                data_list += '<option value="' + resp.list.rows[i].id + '">';
            }

            data_list += '</datalist>';
            swal({
                title: 'What building is this?',
                type: 'info',
                html: data_list,
                showCloseButton: true,
                showCancelButton: true,
                focusConfirm: false,
                confirmButtonText: 'Submit',
                confirmButtonAriaLabel: 'Thumbs up, great!',
                cancelButtonText: 'I don\'t know',
            }).then(function () {
                window.location.href = "./builder.html?bc=" + document.getElementById('place_input').value;
            }, function (dismiss) {
                if (dismiss === 'cancel') {
                    swal(
                        'Oh no!',
                        'Look for a poster with the mellowire logo, or ask the office if there is a mellowire map for this building.',
                        'error'
                    );
                }
            })
        }
    });
}

function getMousePos(event) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: mouseEvent.clientX - rect.left,
        y: mouseEvent.clientY - rect.top
    };
}

function getTouchPos(event, id) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: touchEvent.touches[id].clientX - rect.left,
        y: touchEvent.touches[id].clientY - rect.top
    };
}

function spit() {
    console.log(JSON.stringify(metadata));
}

function upgrade(data) {
    metadata.floor = data;
    editor.set(metadata);
}

init();
